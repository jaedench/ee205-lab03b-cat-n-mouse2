#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

#echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF

echo OK cat, I’m thinking of a number from 1 to $THE_MAX_VALUE.

A_GUESS=0

while [ $A_GUESS -ne $THE_NUMBER_IM_THINKING_OF ]
do
   echo Make a guess:
   read A_GUESS
   if [[ $A_GUESS -lt 1 ]]
   then
      echo "You must enter a number that's >= 1"
   elif [[ $A_GUESS -gt $THE_MAX_VALUE ]]
   then
      echo "You must enter a number that's <= $THE_MAX_VALUE"
   elif [[ $A_GUESS -gt $THE_NUMBER_IM_THINKING_OF ]]
   then
      echo "No cat... the number I'm thinking of is smaller than $A_GUESS"
   elif [[ $A_GUESS -lt $THE_NUMBER_IM_THINKING_OF ]]
   then
      echo "No cat... the number I'm thinking of is larger than $A_GUESS"
   fi
done

echo You got me!
echo " /\\_/\\"
echo "( o.o )"
echo  " > ^ <"

